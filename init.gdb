handle SIGSEGV nostop noprint nopass
break dbg_panic_halt
break hard_shutdown
break bootstrap
set gdb_wait = 0

#b context.c:39
#b kmain.c:254
#b kmain.c:413
#b kmain.c:234
#display *curproc

#switch happens here

#b pframe.c:622
#b faber_test.c:446
#b faber_test.c:450

#b faber_test.c:225
#b faber_test.c:464
b faber_test.c:467
b kmain.c:392

tui enable
focus cmd

#continue
